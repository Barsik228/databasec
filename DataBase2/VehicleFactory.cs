﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase2
{
    class VehicleFactory
    {
        public static Vehicle fromString(string data){
            var parametrs = data.Split(' ');
            Vehicle vehicle = null;
            if (parametrs[0] == "Bicycle")
            {
                vehicle = new Bicycle(Int32.Parse(parametrs[1]), parametrs[2], Int32.Parse(parametrs[3]));
            }

            return vehicle;
        }
    }
}
