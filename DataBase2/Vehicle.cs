﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DataBase2
{
    public abstract class Vehicle
    {
        private string name;
        public string Name { get; set; }
        protected Vehicle(string name)
        {
            this.name = name;
        }

        public abstract void save(string path, Vehicle vehicle);

        public abstract Vehicle load(string path);
    }
}
