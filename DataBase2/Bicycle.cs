﻿using System;
using System.IO;

namespace DataBase2
{
    public class Bicycle : Vehicle
    {
        private int power;
        public int Power { get; set; }
        private string name;
        public string Name { get; set; }
        private int wheels;
        public int Wheels { get; set; }
        
        public Bicycle(int power, string name, int wheels) : base(name)
        {
            this.power = power;
            this.wheels = wheels;
        }
        public override string ToString()
        {
            return "Bicycle" + " " + power + " " + name + " " + wheels;
        }
        public override void save(string path, Bicycle bicycle)
        {
            StreamWriter file = new StreamWriter(path);
            file.WriteLine(bicycle.ToString());
        }

        public override Bicycle load(string path)
        {
            StreamReader file = new StreamReader(path);
            string data = file.ReadLine();
            return (Bicycle)VehicleFactory.fromString(data);
        }
    }
}
